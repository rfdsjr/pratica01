### Comando commit
- Faz o Registro uma nova versão do arquivo dentro do repositório e possibilita criar uma mensagem para descrever as alterações do commit.
- Exemplo: git commit -m "feat: nova adição"